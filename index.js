const express = require("express");
const mongoose = require('mongoose');
const mongoURI = require("./config/keys.js").mongoURI;

require('./services/passport');

mongoose.connect(mongoURI);

const app = express();

require('./routes/authRoutes')(app);

app.get("/", (req, res) => {
  res.send("Server accepting requests!");
});

const PORT = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 5000;
const IP_ADDRESS = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
app.listen(PORT, IP_ADDRESS, () => {
  console.log( "Listening on " + IP_ADDRESS + ", port " + PORT );
});
